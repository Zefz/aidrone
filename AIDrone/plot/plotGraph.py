import numpy as np
import matplotlib.pyplot as plt
import csv

##if len(sys.argv) != 1:
##    print 'Usage: plotGraph filename'
##    exit(-1)

scalefactors=[1.0] + [0.01]*3 + [1.0/1000]*3 + [0.001] +[40.0]*3+ [1.0] +[0.01]*2
offset=[0.0]*12+[-320, -184]
def readfile(filename):
    """arguments: filename - csv file containing data
returns a touple containg a list of the names of the collums, and a 2d
numpy array, where each row contains the data from the corresponding
collum in the csv file."""
    def removeNaN(val):
        return 0.0 if val!=val else val
    
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        names=[name for name in reader.next()]
        data=np.array([[removeNaN(float(val)) for val in line] for line in reader])
    return (names, data.transpose())


                    
def plot(names, data, rows_to_print,scale=scalefactors, offset=offset):
    """plots the given the data, with lables from names
the first row is used as the x coordinates for all data.
rows_to_print is a list of the indices of the rows that are to be printed"""
    for i in rows_to_print:
        plt.plot(data[0],(data[i]+offset[i])*scale[i],label=names[i])
    plt.legend()
    plt.show()

def plot_all(filename):
    (names, data)=readfile(filename)
    plot(names,data,range(len(names)))
