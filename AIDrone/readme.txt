This simple app reads image from the AR Drone 2.0 and displays it on the screen.
If you click an object with distinct color, it will start to search for a largest object of that color in the image.
Pressing R makes the app forget the tracked color.

Q and A are takeoff and land, keypad numbers move the drone.
Z,X changes a camera.
Pressing ENTER saves current image. 

COMPILING
The soft needs standard libraries, SDL-devel 2.0 lib for GUI and ffmpeg (libavutil,libavcodec,libswscale,libz) for video processing.

ISSUES

Make sure your firewall accepts UDP and TCP connections on relevant ports - i.e. 5557 for UDP and 5555 for TPC.
The image bottom 8 rows part are incorrectly decoded - I have left them out for now.
