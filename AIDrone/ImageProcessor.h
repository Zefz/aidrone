#ifndef IMAGEPROCESSING_H
#define IMAGEPROCESSING_H

#include <memory>
#include <SDL2/SDL.h>
#include <opencv2/opencv.hpp>

class ImageProcessor
{
    public:
        ImageProcessor();
        virtual ~ImageProcessor();

        cv::Point2f threshold(const cv::Mat &image, cv::Mat &result);
        cv::Mat hough(const cv::Mat &src);

        void setHoverColor(Uint8 red, Uint8 green, Uint8 blue);

    private:
        Uint8 _red, _green, _blue;
};

#endif // IMAGEPROCESSING_H
