#ifndef NAVDATA_H_INCLUDED
#define NAVDATA_H_INCLUDED

#include <SDL2/SDL_net.h>
#include <thread>
#include <fstream>

struct matrix33_t
{
    float m11;
    float m12;
    float m13;
    float m21;
    float m22;
    float m23;
    float m31;
    float m32;
    float m33;
};

struct vector31_t
{
    union {
        float v[3];
        struct
        {
            float x;
            float y;
            float z;
        } vector3_s;
    };
};

typedef struct _navdata_option_t
{
    uint16_t  tag;
    uint16_t  size;

    uint8_t   *data;
} navdata_option_t;

typedef struct _navdata_t
{
    uint32_t    header;
    uint32_t    drone_state;
    uint32_t    sequence;
    int      vision_defined;

    navdata_option_t  options[1];
} __attribute__ ((packed)) navdata_t;

typedef struct _navdata_cks_t
{
    uint16_t  tag;
    uint16_t  size;

    // Checksum for all navdatas (including options)
    uint32_t  cks;
} __attribute__ ((packed)) navdata_cks_t;

typedef struct _navdata_demo_t
{
    uint16_t    tag;
    uint16_t    size;

    uint32_t    ctrl_state;             /*!< instance of #def_DRONESTATE_state_mask_t */
    uint32_t    vbat_flying_percentage; /*!< battery voltage filtered (mV) */

    float   theta;                  /*!< UAV's attitude */
    float   phi;                    /*!< UAV's attitude */
    float   psi;                    /*!< UAV's attitude */

    int32_t     altitude;               /*!< UAV's altitude */

    float   vx;                     /*!< UAV's estimated linear velocity */
    float   vy;                     /*!< UAV's estimated linear velocity */
    float   vz;                     /*!< UAV's estimated linear velocity */

    uint32_t    num_frames;			  /*!< streamed frame index */

    // Camera parameters compute by detection
    matrix33_t  detection_camera_rot;
    matrix33_t  detection_camera_homo;
    vector31_t  detection_camera_trans;

    // Camera parameters compute by drone
    matrix33_t  drone_camera_rot;
    vector31_t  drone_camera_trans;
} __attribute__ ((packed)) navdata_demo_t;

typedef struct _navdata_iphone_angles_t
{
    uint16_t   tag;
    uint16_t   size;

    int32_t    enable;
    float  ax;
    float  ay;
    float  az;
    uint32_t   elapsed;
} __attribute__ ((packed)) navdata_iphone_angles_t;

typedef struct _navdata_time_t
{
    uint16_t  tag;
    uint16_t  size;

    uint32_t  time;
} __attribute__ ((packed)) navdata_time_t;

typedef struct _navdata_vision_detect_t
{
    uint16_t   tag;
    uint16_t   size;

    uint32_t   nb_detected;
    uint32_t   type[4];
    uint32_t   xc[4];
    uint32_t   yc[4];
    uint32_t   width[4];
    uint32_t   height[4];
    uint32_t   dist[4];
} __attribute__ ((packed)) navdata_vision_detect_t;

struct navdata_unpacked_t
{
    Uint32  drone_state;
    Sint32  vision_defined;
    navdata_demo_t           navdata_demo;
    navdata_iphone_angles_t  navdata_iphone_angles;
    navdata_vision_detect_t  navdata_vision_detect;
};

enum DroneStateBits
{
    DRONESTATE_FLY_MASK            = 1 << 0, /*!< FLY MASK : (0) DRONESTATE is landed, (1) DRONESTATE is flying */
    DRONESTATE_VIDEO_MASK          = 1 << 1, /*!< VIDEO MASK : (0) video disable, (1) video enable */
    DRONESTATE_VISION_MASK         = 1 << 2, /*!< VISION MASK : (0) vision disable, (1) vision enable */
    DRONESTATE_CONTROL_MASK        = 1 << 3, /*!< CONTROL ALGO : (0) euler angles control, (1) angular speed control */
    DRONESTATE_ALTITUDE_MASK       = 1 << 4, /*!< ALTITUDE CONTROL ALGO : (0) altitude control inactive (1) altitude control active */
    DRONESTATE_USER_FEEDBACK_START = 1 << 5, /*!< USER feedback : Start button state */
    DRONESTATE_COMMAND_MASK        = 1 << 6, /*!< Control command ACK : (0) None, (1) one received */
    DRONESTATE_TRIM_COMMAND_MASK   = 1 << 7, /*!< Trim command ACK : (0) None, (1) one received */
    DRONESTATE_TRIM_RUNNING_MASK   = 1 << 8, /*!< Trim running : (0) none, (1) running */
    DRONESTATE_TRIM_RESULT_MASK    = 1 << 9, /*!< Trim result : (0) failed, (1) succeeded */
    DRONESTATE_NAVDATA_DEMO_MASK   = 1 << 10, /*!< Navdata demo : (0) All navdata, (1) only navdata demo */
    DRONESTATE_NAVDATA_BOOTSTRAP   = 1 << 11, /*!< Navdata bootstrap : (0) options sent in all or demo mode, (1) no navdata options sent */
    DRONESTATE_MOTORS_BRUSHED      = 1 << 12, /*!< Motors brushed : (0) brushless, (1) brushed */
    DRONESTATE_COM_LOST_MASK       = 1 << 13, /*!< Communication Lost : (1) com problem, (0) Com is ok */
    DRONESTATE_GYROS_ZERO          = 1 << 14, /*!< Bit means that there's an hardware problem with gyrometers */
    DRONESTATE_VBAT_LOW            = 1 << 15, /*!< VBat low : (1) too low, (0) Ok */
    DRONESTATE_VBAT_HIGH           = 1 << 16, /*!< VBat high (US mad) : (1) too high, (0) Ok */
    DRONESTATE_TIMER_ELAPSED       = 1 << 17, /*!< Timer elapsed : (1) elapsed, (0) not elapsed */
    DRONESTATE_NOT_ENOUGH_POWER    = 1 << 18, /*!< Power : (0) Ok, (1) not enough to fly */
    DRONESTATE_ANGLES_OUT_OF_RANGE = 1 << 19, /*!< Angles : (0) Ok, (1) out of range */
    DRONESTATE_WIND_MASK           = 1 << 20, /*!< Wind : (0) Ok, (1) too much to fly */
    DRONESTATE_ULTRASOUND_MASK     = 1 << 21, /*!< Ultrasonic sensor : (0) Ok, (1) deaf */
    DRONESTATE_CUTOUT_MASK         = 1 << 22, /*!< Cutout system detection : (0) Not detected, (1) detected */
    DRONESTATE_PIC_VERSION_MASK    = 1 << 23, /*!< PIC Version number OK : (0) a bad version number, (1) version number is OK */
    DRONESTATE_ATCODEC_THREAD_ON   = 1 << 24, /*!< ATCodec thread ON : (0) thread OFF (1) thread ON */
    DRONESTATE_NAVDATA_THREAD_ON   = 1 << 25, /*!< Navdata thread ON : (0) thread OFF (1) thread ON */
    DRONESTATE_VIDEO_THREAD_ON     = 1 << 26, /*!< Video thread ON : (0) thread OFF (1) thread ON */
    DRONESTATE_ACQ_THREAD_ON       = 1 << 27, /*!< Acquisition thread ON : (0) thread OFF (1) thread ON */
    DRONESTATE_CTRL_WATCHDOG_MASK  = 1 << 28, /*!< CTRL watchdog : (1) delay in control execution (> 5ms), (0) control is well scheduled */
    DRONESTATE_ADC_WATCHDOG_MASK   = 1 << 29, /*!< ADC Watchdog : (1) delay in uart2 dsr (> 5ms), (0) uart2 is good */
    DRONESTATE_COM_WATCHDOG_MASK   = 1 << 30, /*!< Communication Watchdog : (1) com problem, (0) Com is ok */
    DRONESTATE_EMERGENCY_MASK      = 1 << 31  /*!< Emergency landing : (0) no emergency, (1) emergency */
};

class Navdata
{
    public:
        Navdata();
        virtual ~Navdata();

        void printNavdataInfo(float leftRightTilt, float frontBackTilt, float verticalSpeed, float angularSpeed, float lastMarkerX, float lastMarkerY);
        bool hasState(DroneStateBits bit);
        bool hasChanged();
        void pingNavdata();

    private:
        void navdataLoop();
        void decodeNavdataPacket(navdata_t* navdata, Uint32* checksum);

    //Current drone telemetry
    public:
        float vx, vy, vz;

        float phi;
        float theta;
        float psi;
        float altitude;

        float battery;

    private:
        std::thread _navdataThread;
        bool _terminateThread;
        UDPsocket _navdataSocket;

        Uint32 _droneState;

        std::ofstream _logStatistics;
        bool _navdataChanged;
};

extern uint32_t mykonos_state;

#endif // NAVDATA_H_INCLUDED
