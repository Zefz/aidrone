#include "ImageProcessor.h"

ImageProcessor::ImageProcessor()
{
    //Set som arbitrary default values (determined by empirical testing)
    _red = 141;
    _green = 35;
    _blue = 41;
}

ImageProcessor::~ImageProcessor()
{

}

/**
* Hough transform... currently unused
**/
cv::Mat ImageProcessor::hough(const cv::Mat &src)
{
    cv::Mat src_gray;
    cv::Mat result = src.clone();

    /// Convert it to gray
    cv::cvtColor(src, src_gray, CV_BGR2GRAY);

    /// Reduce the noise so we avoid false circle detection
    cv::GaussianBlur(src_gray, src_gray, cv::Size(9, 9), 2, 2);

    std::vector<cv::Vec3f> circles;

    /// Apply the Hough Transform to find the circles
    cv::HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0 );

    /// Draw the circles detected
    for( size_t i = 0; i < circles.size(); i++ )
    {
        cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        int radius = cvRound(circles[i][2]);
        // circle center
        cv::circle(result, center, 3, cv::Scalar(0,255,0), -1, 8, 0 );
        // circle outline
        cv::circle(result, center, radius, cv::Scalar(0,0,255), 3, 8, 0 );
    }

    return result;
}

/**
* Turns a RGB image into a binary image with moments calculated on the biggest blob of RGB color
**/
cv::Point2f ImageProcessor::threshold(const cv::Mat &src, cv::Mat &redOnly)
{
    const int THRESHOLD = 60;
    cv::inRange(src, cv::Scalar(_red-THRESHOLD, _green-THRESHOLD, _blue-THRESHOLD), cv::Scalar(_red+THRESHOLD, _green+THRESHOLD, _blue+THRESHOLD), redOnly);

    int morph_size = 6;
    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size( 2*morph_size + 1, 2*morph_size+1 ), cv::Point( morph_size, morph_size ));

    //Apply the specified morphology operation
    cv::morphologyEx(redOnly, redOnly, cv::MORPH_OPEN, element);

    //Calculate moment
    cv::Moments moment = cv::moments(redOnly, true);

    //Convert image to 3 channels and draw red dot where the circle is
    cv::cvtColor(redOnly, redOnly, CV_GRAY2RGB);
    cv::Point2f centre = cv::Point2f( moment.m10/moment.m00 , moment.m01/moment.m00 );
    cv::circle(redOnly, centre, 4, cv::Scalar(255, 0, 0), -1, 8, 0 );

    return centre;
}

/**
* Determines what RGB color we are trying to hover over
**/
void ImageProcessor::setHoverColor(Uint8 red, Uint8 green, Uint8 blue)
{
    _red = red;
    _green = green;
    _blue = blue;
}
