#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H
#include <SDL2/SDL.h>

class PIDController
{
    public:
        PIDController(double kProp, double kIntegral, double kDeriv);
        virtual ~PIDController();
        double controllerOutput(double error);
        double controllerOutputAtTime(double error, Uint32 time);
    protected:
    private:
        double _kProp, _kDeriv, _kIntegral;
        double _integral, _prevError;
        Uint32 _prevTime;
        bool _running;
};

#endif // PIDCONTROLLER_H
