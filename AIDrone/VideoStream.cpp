#include "DroneConstants.h"
#include "VideoStream.h"

static const int PAVE_HEADER_LENGTH = 68;

//The famous PaVE header data structure
typedef struct
{
	uint8_t signature[4]; /* "PaVE" - used to identify the start of
				 frame */
	uint8_t version; /* Version code */
	uint8_t video_codec; /* Codec of the following frame */
	uint16_t header_size; /* Size of the parrot_video_encapsulation_t
				 */
	uint32_t payload_size; /* Amount of data following this PaVE */
	uint16_t encoded_stream_width; /* ex: 640 */
	uint16_t encoded_stream_height; /* ex: 368 */
	uint16_t display_width; /* ex: 640 */
	uint16_t display_height; /* ex: 360 */
	uint32_t frame_number; /* Frame position inside the current stream
				  */
	uint32_t timestamp; /* In milliseconds */
	uint8_t total_chuncks; /* Number of UDP packets containing the
				  current decodable payload - currently unused */
	uint8_t chunck_index ; /* Position of the packet - first chunk is #0
				  - currenty unused*/
	uint8_t frame_type; /* I-frame, P-frame -
			       parrot_video_encapsulation_frametypes_t */
	uint8_t control; /* Special commands like end-of-stream or
			    advertised frames */
	uint32_t stream_byte_position_lw; /* Byte position of the current payload in
					     the encoded stream - lower 32-bit word */
	uint32_t stream_byte_position_uw; /* Byte position of the current payload in
					     the encoded stream - upper 32-bit word */
	uint16_t stream_id; /* This ID indentifies packets that should be
			       recorded together */
	uint8_t total_slices; /* number of slices composing the current
				 frame */
	uint8_t slice_index ; /* position of the current slice in the frame
				 */
	uint8_t header1_size; /* H.264 only : size of SPS inside payload -
				 no SPS present if value is zero */
	uint8_t header2_size; /* H.264 only : size of PPS inside payload -
				 no PPS present if value is zero */
	uint8_t reserved2[2]; /* Padding to align on 48 bytes */
	uint32_t advertised_size; /* Size of frames announced as advertised
				     frames */
	uint8_t reserved3[12]; /* Padding to align on 64 bytes */
} __attribute__ ((packed)) parrot_video_encapsulation_t;


VideoStream::VideoStream()
{
	_terminateThread = false;
	_frameHasChanged = false;

	_videoSocket = nullptr;

    //Image resolution
	_srcX = _dstX = 640;
	_srcY = _dstY = 368;

    //Allocate image to hold the current frame
	_currentFrame = cvCreateImage(cvSize(_dstX, _dstY), IPL_DEPTH_8U, 3);   //IPL_DEPTH_8U = 8-bit unsigned

	//Get codec
    avcodec_register_all();
	av_init_packet(&_avpkt);
	_codec = avcodec_find_decoder(CODEC_ID_H264);
	if (!_codec)
	{
		printf("ERROR: Video codec not found!\n");
		exit(-1);
	}

	_c = avcodec_alloc_context3(_codec);
	_picture = avcodec_alloc_frame();
	_pictureRGB = avcodec_alloc_frame();

	_img_convert_ctx = sws_getContext(_srcX, _srcY, PIX_FMT_YUV420P, _dstX, _dstY, PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);
	//if(codec->capabilities&CODEC_CAP_TRUNCATED) c->flags|= CODEC_FLAG_TRUNCATED;
	if (avcodec_open2(_c, _codec, NULL) < 0)
	{
		printf("ERROR: Could not open codec\n");
		exit(-1);
	}
	avpicture_fill((AVPicture *)_pictureRGB, (Uint8*)_currentFrame.data, PIX_FMT_RGB24, _dstX, _dstY);

    //Start the network streaming thread
    _thread = std::thread(&VideoStream::videoLoop, this);
}

VideoStream::~VideoStream()
{
	_terminateThread = true;
	SDLNet_TCP_Close(_videoSocket);
	_videoSocket = nullptr;
	_thread.join();

    //Free codec
	sws_freeContext(_img_convert_ctx);
	avcodec_close(_c);
	av_free(_c);
	av_free(_picture);
	av_free(_pictureRGB);
}

/**
* Gets the latest video frame that has been decoded as an OpenCV matrix.
**/
cv::Mat VideoStream::getCurrentFrame()
{
    return _currentFrame;
}

/**
* This helper function allows us to do a blocking TCP read (it can return early if the socket
* is closed or an error occurs)
**/
int VideoStream::receiveBlocking(TCPsocket socket, Uint8 *byteBuffer, const int bytesToRead)
{
    int totalBytesReceived = 0;

    while(bytesToRead > totalBytesReceived && !_terminateThread)
    {
        int bytesReceived = SDLNet_TCP_Recv(socket, &byteBuffer[totalBytesReceived], bytesToRead-totalBytesReceived);
        if(bytesReceived <= 0)
        {
            const char* message = (bytesReceived == 0) ? "Connection closed" : SDLNet_GetError();
            printf("TCP socket problem receiving: %s\n", message);
            if(_videoSocket)
            {
                SDLNet_TCP_Close(_videoSocket);
                _videoSocket = nullptr;
            }
            return totalBytesReceived; //error or connection has closed
        }
        totalBytesReceived += bytesReceived;
    }

//    printf("Success! got %d bytes!\n", totalBytesReceived);
    return totalBytesReceived;
}

/**
* Main video stream loop. Should be called in a separate thread. This will read incoming data from TCP and
* decode each frame. It provides automatic recovery if the stream should be broken somehow.
**/
void VideoStream::videoLoop()
{
	parrot_video_encapsulation_t pave;
	Uint8 *byteBuffer = new Uint8[1024*1024]; //1 mb should be enough?

    //Resolve IP address
    IPaddress droneIP;
    if(SDLNet_ResolveHost(&droneIP, DRONE_IP_ADDRESS, VIDEO_STREAM_PORT) < 0)
    {
        printf("Unable to resolve video stream at %s:%d\n", DRONE_IP_ADDRESS, VIDEO_STREAM_PORT);
        return;
    }

    while (!_terminateThread)
    {

        //Ensure we have established connection
        if(!_videoSocket)
        {
            //Open a listening TCP socket
            printf("Waiting for video stream to establish...\n");
            _videoSocket = SDLNet_TCP_Open(&droneIP);
            if(!_videoSocket)
            {
                printf("Unable to open TCP socket: %s\n", SDLNet_GetError());
                SDL_Delay(1000);
                continue;
            }
            printf("Video stream established!\n");
            _streamTimeStart = SDL_GetTicks();
            _framesReceived = 0;
        }

        //Decode header
        if(receiveBlocking(_videoSocket, byteBuffer, PAVE_HEADER_LENGTH) < PAVE_HEADER_LENGTH) continue;
        if(byteBuffer[0] != 'P' || byteBuffer[1] != 'a' || byteBuffer[2] != 'V' || byteBuffer[3] != 'E')
        {
            bool paveDetected = false;
            printf("Video stream sync lost\n");

            while (paveDetected == false && !_terminateThread)
            {
                receiveBlocking(_videoSocket, byteBuffer, 512);
                for(int i = 0; i < 512-PAVE_HEADER_LENGTH; ++i)
                {
                    if(byteBuffer[i+0] != 'P') continue;
                    if(byteBuffer[i+1] != 'a') continue;
                    if(byteBuffer[i+2] != 'V') continue;
                    if(byteBuffer[i+3] != 'E') continue;

                    paveDetected = true;
                    memcpy(&pave, &byteBuffer[i], PAVE_HEADER_LENGTH);
                    break;
                }
            }
        }
        else memcpy(&pave, &byteBuffer[0], PAVE_HEADER_LENGTH);

        //Decode frame
        if(receiveBlocking(_videoSocket, byteBuffer, pave.payload_size) < pave.payload_size) continue;
        decodeFrame(byteBuffer, pave.payload_size, pave.frame_type);
        _frameHasChanged = true;
        _framesReceived++;
    }

    float seconds = (SDL_GetTicks() - _streamTimeStart) / 1000.0f;
    printf("Received a total of %d frames (average %f frames per second)\n", _framesReceived, (_framesReceived/seconds));
	delete byteBuffer;
}

/**
* This function decodes a single video frame
**/
void VideoStream::decodeFrame(Uint8* byteBuffer, const int len, const int type)
{
    int _got_picture;
	_avpkt.size = len;
	_avpkt.data = byteBuffer;
	_avpkt.flags = 0;

	if(type == 1) _avpkt.flags = AV_PKT_FLAG_KEY;

	avcodec_decode_video2(_c, _picture, &_got_picture, &_avpkt);
	if(_got_picture > 0)
	{
		sws_scale(_img_convert_ctx, _picture->data, _picture->linesize, 0, _srcY-8, _pictureRGB->data, _pictureRGB->linesize);
	}
}

/**
* Checks if the video frame has changed since the last call to this function
**/
bool VideoStream::hasChanged()
{
    if(_frameHasChanged)
    {
        _frameHasChanged = false;
        return true;
    }

    return false;
}
