#include "PIDTest.h"
#include "PIDController.h"
#include <SDL2/SDL.h>

PIDTest::PIDTest()
{
    //ctor
}

int main(int argc, char** argv)
{
    PIDController p=PIDController(1.0, 0.0, 0.0);

    SDL_assert(p.controllerOutputAtTime(12.3, 2345)==12.3);
    SDL_assert(p.controllerOutputAtTime(-1.3, 2500)==-1.3);
    SDL_assert(p.controllerOutputAtTime(12.3, 2600)==12.3);
    SDL_assert(p.controllerOutputAtTime(12.9, 2700)==12.9);

    PIDController i=PIDController(0.0, 1.0, 0.0);
    SDL_assert(i.controllerOutputAtTime(10.0, 2000)==0.0);
    SDL_assert(i.controllerOutputAtTime(10.0, 3000)==10.0);
    SDL_assert(i.controllerOutputAtTime(10.0, 3000)==10.0);
    double tmp=i.controllerOutputAtTime(20.0, 3500);
    printf("Assertion 1: %f\n", tmp);
    SDL_assert(tmp==20.0);
    SDL_assert(i.controllerOutputAtTime(0.0, 4000)==20.0);

    PIDController d=PIDController(0.0, 0.0, 1.0);
    SDL_assert(d.controllerOutputAtTime(10.0, 2000)==0.0);
    SDL_assert(d.controllerOutputAtTime(20.0, 3000)==10.0);
    SDL_assert(d.controllerOutputAtTime(10.0, 4000)==-10.0);
    return 0;
}

PIDTest::~PIDTest()
{
    //dtor
}
