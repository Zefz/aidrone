#include <sstream>

#include "DroneCommands.h"
#include "Navdata.h"
#include "DroneConstants.h"

/* Command constants */
#define AT_PORT                   5556      //Port number to send commands on
#define AT_BUFFER_SIZE            1024      //the maximum byte length of a command (hard coded in the drone firmware)


enum DroneControlBits
{
    MYKONOS_UI_BIT_AG             = 0, /* Button turn to left */
    MYKONOS_UI_BIT_AB             = 1, /* Button altitude down (ah - ab)*/
    MYKONOS_UI_BIT_AD             = 2, /* Button turn to right */
    MYKONOS_UI_BIT_AH             = 3, /* Button altitude up (ah - ab)*/
    MYKONOS_UI_BIT_L1             = 4, /* Button - z-axis (r1 - l1) */
    MYKONOS_UI_BIT_R1             = 5, /* Not used */
    MYKONOS_UI_BIT_L2             = 6, /* Button + z-axis (r1 - l1) */
    MYKONOS_UI_BIT_R2             = 7, /* Not used */
    MYKONOS_UI_BIT_SELECT         = 8, /* Button emergency reset all */
    MYKONOS_UI_BIT_START          = 9, /* Button Takeoff / Landing */
    MYKONOS_UI_BIT_TRIM_THETA     = 18, /* y-axis trim +1 (Trim increase at +/- 1??/s) */
    MYKONOS_UI_BIT_TRIM_PHI       = 20, /* x-axis trim +1 (Trim increase at +/- 1??/s) */
    MYKONOS_UI_BIT_TRIM_YAW       = 22, /* z-axis trim +1 (Trim increase at +/- 1??/s) */
    MYKONOS_UI_BIT_X              = 24, /* x-axis +1 */
    MYKONOS_UI_BIT_Y              = 28, /* y-axis +1 */
};

typedef struct _radiogp_cmd_t
{
  float enable;
  float pitch;
  float roll;
  float gaz;
  float yaw;
} radiogp_cmd_t;
static radiogp_cmd_t radiogp_cmd = {0};

#define DEFAULT_BITFIELD						\
    ((1 << MYKONOS_UI_BIT_TRIM_THETA) |			\
	 (1 << MYKONOS_UI_BIT_TRIM_PHI) |			\
	 (1 << MYKONOS_UI_BIT_TRIM_YAW) |			\
	 (1 << MYKONOS_UI_BIT_X) |					\
	 (1 << MYKONOS_UI_BIT_Y))


DroneCommands::DroneCommands(Navdata *navdata)
{
    _navdata = navdata;
    _disableBuiltinHoverMode = false;

    //Create network socket
    _commandSocket = SDLNet_UDP_Open(AT_PORT);
    if(!_commandSocket)
    {
        printf("Unable to create command socket: %s\n", SDLNet_GetError());
        return;
    }

    if(SDLNet_ResolveHost(&_droneAddress, DRONE_IP_ADDRESS, AT_PORT) < 0)
    {
        printf("Unable to find drone: %s\n", SDLNet_GetError());
        return;
    }

    //Start the thread
    _terminateThread = false;
	_commandThread = std::thread(&DroneCommands::commandLoop, this);
}

DroneCommands::~DroneCommands()
{
    _terminateThread = true;
    SDLNet_UDP_Close(_commandSocket);
    if(_commandThread.joinable()) _commandThread.join();
}

/**
* Enables or disables the built-in hover mode of the drone
**/
void DroneCommands::setBuiltInHoverMode(const bool enabled)
{
    _disableBuiltinHoverMode = !enabled;
}

/**
* The main command loop, should be run in separate thread
**/
void DroneCommands::commandLoop()
{
	printf("Commands thread starting...\n");

	while (!_terminateThread)
	{
        //Drone is in bootstrap mode
		if (_navdata->hasState(DRONESTATE_NAVDATA_BOOTSTRAP))
		{
			bootDrone(_sequenceNumber++);
			continue;
		}

        //Communication problem, send ping to reset
		if(_navdata->hasState(DRONESTATE_CTRL_WATCHDOG_MASK))
		{
            printf("Communication problem... resetting\n");
            ping();
            _navdata->pingNavdata();
            continue;
		}

		// compute next loop iteration deadline
		Uint32 deadline = SDL_GetTicks() + REFRESH_MS;

		// send pilot command
		int hoverBit = (radiogp_cmd.pitch == 0 && radiogp_cmd.roll == 0) ? 0 : 1;
		if(_disableBuiltinHoverMode) hoverBit = 1;
		int pitch = *(int*)&radiogp_cmd.pitch;
		int roll = *(int*)&radiogp_cmd.roll;
		int height = *(int*)&radiogp_cmd.gaz;
		int yaw = *(int*)&radiogp_cmd.yaw;
		sendCommand("PCMD", {hoverBit, pitch, roll, height, yaw});

		// sleep until deadline
		Uint32 current = deadline - SDL_GetTicks();
		if(current > REFRESH_MS) printf("ERROR: wait time is > %d (currently is %d)\n", REFRESH_MS, current);
		if (current > 0)
		{
            SDL_Delay(current);
		}
	}

	printf("Commands thread stopping\n");
}

/**
* This tells the drone to initialize from bootstrap mode into normal flying mode
**/
void DroneCommands::bootDrone(int attempt)
{
//	char cmds[1000];
	printf("Drone is in bootstrap mode... Booting\n");
//	sprintf(cmds,"AT*CONFIG=%i,\"general:navdata_demo\",\"TRUE\"\r", _sequenceNumber++);
	setConfig("general:navdata_demo", "TRUE");
	_sequenceNumber += 2;
//	sendCommandRaw(cmds);
	ping();

	//Still not finished bootstrap mode?
	if (_navdata->hasState(DRONESTATE_NAVDATA_BOOTSTRAP))
	{
//		sendCommandRaw(cmds);
        setConfig("general:navdata_demo", "TRUE");

		int retry = 20;
		bool bcontinue = true;
		int next = 0;
		while (bcontinue && retry)
		{
			if (next == 0) {
				if (_navdata->hasState(DRONESTATE_NAVDATA_BOOTSTRAP))
				{
					printf("[CONTROL] Processing the current command ... \n");
					next++;
				}
				else
				{
//					sprintf(cmds,"AT*CONFIG=%i,\"general:navdata_demo\",\"TRUE\"\r", _sequenceNumber++);
//					sendCommandRaw(cmds);
                    setConfig("general:navdata_demo", "TRUE");
				}
				printf("Bootstrap mode finished\n");
			}
			else if (!_navdata->hasState(DRONESTATE_COMMAND_MASK))
            {
                printf("[CONTROL] Ack control event OK, send navdata demo\n");
                bcontinue = false;
            }

			SDL_Delay(100);
			retry--;
		}
	}
	ping();
}

/**
* Sends the specified string command to the drone
**/
void DroneCommands::sendCommandRaw(const std::string &command)
{
    UDPpacket packet;
    packet.address = _droneAddress;
    packet.maxlen = AT_BUFFER_SIZE;
    packet.len = command.length()+1;
    packet.status = 0;
    packet.channel = 0;
    packet.data = (Uint8*)command.c_str();

    _commandMutex.lock();
    SDLNet_UDP_Send(_commandSocket, -1, &packet);
    _commandMutex.unlock();
}

/**
* Resets the communication watchdog
**/
void DroneCommands::ping()
{
    sendCommand("COMDWG");
}

/**
* Description : Calibration of the ARDrone.
**/
void DroneCommands::trim()
{
	sendCommand("FTRIM");
	SDL_Delay(100);
	setConfig("control:altitude_max", 5000);
}

/**
* Sets a configuration of the drone
**/
template<typename T>
void DroneCommands::setConfig(const std::string &name, T value)
{
    std::stringstream stringBuilder;
    stringBuilder << "AT*CONFIG=" << _sequenceNumber++ << ",\"" << name << "\",\"" << value << "\"\r";
    sendCommandRaw(stringBuilder.str());
    //printf("%s\n", stringBuilder.str().c_str());
}

/**
* Enable or disable the built-in roundel detection
**/
void DroneCommands::setRoundelDetectionEnabled(bool enabled)
{
    printf("Hover mode is %s\n", enabled ? "enabled" : "disabled");
	setConfig("detect:detect_type", enabled ? CAD_TYPE_ORIENTED_COCARDE_BW : CAD_TYPE_NONE);
	SDL_Delay(100);
	setConfig("control:flying_mode", enabled ? FLYING_MODE_HOVER_ON_TOP_OF_ROUNDEL : FLYING_MODE_FREE_FLIGHT);
	SDL_Delay(100);
	if(enabled) setConfig("control:hovering_mode", 5000);
}

/**
* Sets config which camera should be streamed
**/
void DroneCommands::setCamera(int cam)
{
	setConfig("video:video_channel", cam);
}

/**
* Tells the drone to rename the SSID of it's WiFi
**/
void DroneCommands::setWifiName(const char* name)
{
	setConfig("network:ssid_single_player", name);
}

/**
* Sends a command with an arbitrary length of integers (floats are sent as integers to the drone)
**/
void DroneCommands::sendCommand(const std::string &type, std::initializer_list<int> args)
{
    std::stringstream stringBuilder;

    bool first = true;
    stringBuilder << "AT*" << type << '=' << _sequenceNumber++ << ',';
    for(int i : args)
    {
        if(!first) stringBuilder << ',';
        stringBuilder << i;
        first = false;
    }
    stringBuilder << '\r';

	sendCommandRaw(stringBuilder.str());
}

/**
* Tells the drone to takeoff
**/
void DroneCommands::takeoff()
{
    Uint32 bitField = DEFAULT_BITFIELD;
    bitField |= (1 << MYKONOS_UI_BIT_START);
	sendREFCommand(bitField);
}

/**
* Tells the droen to land
**/
void DroneCommands::land()
{
    Uint32 bitField = DEFAULT_BITFIELD;
	sendREFCommand(bitField);
}

/**
* Tells the drone how it should fly the next update loop
**/
void DroneCommands::setPilotInput(float pitch, float roll, float height, float yaw)
{
	_commandMutex.lock();
	radiogp_cmd.enable = 1.0;
	radiogp_cmd.pitch = pitch;
	radiogp_cmd.roll = roll;
	radiogp_cmd.gaz = -height;
	radiogp_cmd.yaw = yaw;
	_commandMutex.unlock();
}

/**
* Makes the drone enter or exit emergency mode
**/
void DroneCommands::emergencyStop()
{
    Uint32 bitField = DEFAULT_BITFIELD;
    bitField |= (1 << MYKONOS_UI_BIT_SELECT);
	sendREFCommand(bitField);
}

/**
* Sends a REF command with the specified bitfield
**/
void DroneCommands::sendREFCommand(Uint32 bitField)
{
    std::stringstream stringBuilder;
    stringBuilder << "AT*REF=" << _sequenceNumber++ << ',' << bitField << '\r';
    sendCommandRaw(stringBuilder.str());
}
