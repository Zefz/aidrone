#ifndef AIDRONE_H
#define AIDRONE_H

//This code snippet adds FFMPEG compatability to 64-bit C++ compiler (else you will get compile error in common.h)
#ifndef INT64_C
#define INT64_C(c) (c ## LL)
#define UINT64_C(c) (c ## ULL)
#endif

#include <SDL2/SDL.h>
#include "ImageProcessor.h"

//Forward declarations
class VideoStream;
class Navdata;
class DroneCommands;
class PIDController;

class AIDrone
{
    public:
        AIDrone();
        virtual ~AIDrone();

        void update();
        void renderFrame();
        bool requestsExit();

        // chooses the camera - 0-front, 1-bottom, 2,3-picture in picture mode
        void switchCamera(int cam);

        //resets the drone state, recalibrates sensors and takes off
        void takeoff();

        //lands the drone
        void land();

        //sets the pitch, roll, yaw and vertical speed
        void setAngles(float pitch, float roll, float yaw, float vertical);

    private:
        void runPIDRegulator();

    private:
        bool _requestExit;
        bool _loggerEnabled;

        //Drone subsystems
        Navdata *_navdata;
        DroneCommands *_commands;
        VideoStream *_videoStream;
        ImageProcessor _imageProcessor;

        float pitch, roll, yaw, height;

        //Rendering stuff
        SDL_Window* _screen;
        SDL_Renderer* _renderer;
        SDL_Texture* _texture;
        //TTF_Font *_font;

        //Hover mode data
        cv::Point2f _lastObservedHoverPoint;
        bool _showImageProcessing;

        //PID regulators
        bool _enablePIDRegulator;
        PIDController *pidX;
        PIDController *pidY;
        bool _isNanDetected;
};

#endif // AIDRONE_H
