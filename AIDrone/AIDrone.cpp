#include <SDL2/SDL_net.h>

#include "AIDrone.h"
#include "DroneCommands.h"
#include "DroneConstants.h"
#include "Navdata.h"
#include "VideoStream.h"
#include "PIDController.h"

AIDrone::AIDrone()
{
    _navdata = nullptr;
    _requestExit = false;
    _enablePIDRegulator = false;
    _showImageProcessing = false;
    _loggerEnabled = false;

    //Init pid regulators
    pidX = new PIDController(0.00002, 0.0, 0.002);
    pidY = new PIDController(0.00002, 0.0, 0.002);

    //Initialize the hover point
    _lastObservedHoverPoint.x = _lastObservedHoverPoint.y = 0;
    _isNanDetected = true;

	//initializing stuff
	_videoStream = new VideoStream();

    //Initializes the video subsystem *must be done before anything other!!
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("Unable to init SDL: %s\n", SDL_GetError());
        exit(-1);
    }

    if(SDLNet_Init() < 0)
    {
        printf("Problem initializing SDL_Net: %s\n", SDLNet_GetError());
        exit(-1);
    }

/*    if(TTF_Init() < 0)
    {
        printf("Problem initializing SDL_TTF: %s\n", TTF_GetError());
        exit(-1);
    }
    _font = TTF_OpenFont("./font/arial.ttf", 24);*/

    //Open a window
    _screen = SDL_CreateWindow("Simple AR-drone vision system", 0, 0, 640, 368, SDL_WINDOW_SHOWN);
    if (!_screen)
        printf("Couldn't set SDL video mode: %s\n",SDL_GetError());

    _renderer = SDL_CreateRenderer(_screen, -1, SDL_RENDERER_ACCELERATED);
    if (!_renderer)
        printf("Couldn't create renderer: %s\n",SDL_GetError());

    _texture = SDL_CreateTexture(_renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, 640, 368);
    if(!_texture)
        printf("ERROR: Unable to create texture: %s\n", SDL_GetError());
    void *pixels;
    int pitch;
    SDL_LockTexture(_texture, nullptr, &pixels, &pitch);
    memset(pixels, 0, 640 * 368 * 3); //24 bytes per pixel (8 byte RGB)
    SDL_UnlockTexture(_texture);

    //Initialize drone subsystems
    _navdata = new Navdata();
    _commands = new DroneCommands(_navdata);
}

AIDrone::~AIDrone()
{
    delete pidX;
    delete pidY;

    printf("waiting for video to close\n");
	delete _videoStream;
    printf("waiting for navdata to close\n");
    delete _navdata;
    printf("waiting for commands to close\n");
    delete _commands;
    printf("goodbye!\n");

    SDL_DestroyTexture(_texture);
    SDL_DestroyRenderer(_renderer);
    SDL_DestroyWindow(_screen);
    SDL_Quit();
    SDLNet_Quit();
}

/**
* Tells the drone which camera it should stream
**/
void AIDrone::switchCamera(int cam)
{
    _commands->setCamera(cam);
}

/**
* Runs the PID regulator and uses the results to tell the drone how it should behave
**/
void AIDrone::runPIDRegulator()
{
    //Don't act on invalid data
    if(!_enablePIDRegulator || _isNanDetected) return;

    const int SETPOINT_X = 320;
    const int SETPOINT_Y = 184;

    double outputX = - pidX->controllerOutput(SETPOINT_X-_lastObservedHoverPoint.x);
    double outputY = - pidY->controllerOutput(SETPOINT_Y-_lastObservedHoverPoint.y);

    roll = outputX;
    pitch = outputY;

    if(outputX != 0)
    {
        printf("%s = %f\n", outputX<0 ? "Move left" : "Move right", outputX);
    }
    if(outputY != 0)
    {
        printf("%s = %f\n", outputY>0 ? "Backwards" : "Forewards", outputY);
    }

    if(abs(roll) > 3000 || abs(pitch) > 3000)
    {
        roll = 0;
        pitch = 0;
        printf("Override emergency slow\n");
    }
}

/**
* The update function for the drone. Here SDL events are handled, logging is done and where the drone
* autonomous AI is run.
**/
void AIDrone::update()
{
    SDL_Event event;

    //do the regulator stuff
    runPIDRegulator();

    //Poll SDL events
    while (SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            //Window closed
            case SDL_QUIT:
                _requestExit = true;
            break;

            case SDL_KEYUP:
                pitch = roll = yaw = height = 0.0f;
            break;

            case SDL_MOUSEBUTTONDOWN:
            {
                cv::Mat currentFrame = _videoStream->getCurrentFrame();
                cv::Vec3b pixel = currentFrame.at<cv::Vec3b>(event.button.y, event.button.x);
                printf("R:%d,G:%d,B:%d\n", pixel[0], pixel[1], pixel[2]);
                _imageProcessor.setHoverColor(pixel[0], pixel[1], pixel[2]);
            }
            break;

            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        _commands->land();
                        _requestExit = true;
                    break;

                    case SDLK_SPACE:
                        _showImageProcessing = !_showImageProcessing;
                    break;

                    case SDLK_LALT:
                        _enablePIDRegulator = !_enablePIDRegulator;
                        printf("Hovermode is %s\n", _enablePIDRegulator ? "ENABLED" : "DISABLED");
                        _commands->setBuiltInHoverMode(!_enablePIDRegulator);
                    break;

                    case SDLK_TAB:
                        _commands->emergencyStop();
                    break;

                    case SDLK_KP_7:
                        yaw = -0.60f;
                    break;

                    case SDLK_KP_9:
                        yaw = 0.60f;
                    break;

                    case SDLK_KP_4:
                        roll = -0.60f;
                    break;

                    case SDLK_KP_6:
                        roll = 0.60f;
                    break;

                    case SDLK_KP_8:
                        pitch = -0.60f;
                    break;

                    case SDLK_KP_2:
                        pitch = 0.60f;
                    break;

                    case SDLK_KP_PLUS:
                        height = 0.60f;
                    break;

                    case SDLK_KP_MINUS:
                        height = -0.60f;
                    break;

                    case SDLK_1:
                        switchCamera(0);
                    break;
                    case SDLK_2:
                        switchCamera(1);
                    break;
                    case SDLK_3:
                        switchCamera(2);
                    break;
                    case SDLK_4:
                        switchCamera(3);
                    break;

                    case SDLK_q:
                        takeoff();
                    break;

                    case SDLK_a:
                        land();
                    break;

                    case SDLK_y:
                        static bool detectionEnabled = false;
                        detectionEnabled = !detectionEnabled;
                        _commands->setRoundelDetectionEnabled(detectionEnabled);
                    break;

                    case SDLK_l:
                        _loggerEnabled = !_loggerEnabled;
                    break;

                    default:
                        //ignored (so no warnings for missing cases are shown)
                    break;
                }
            break;

        }
    }

    setAngles(pitch,roll,yaw,height);

    //Print some navdata
    if(_loggerEnabled && _navdata->hasChanged())
    {
        _navdata->printNavdataInfo(roll, pitch, height, yaw, _lastObservedHoverPoint.x, _lastObservedHoverPoint.y);
    }
}

/**
* Small helper function to clamp a float (A) between positive and negative of a value (B)
**/
inline float saturation(float a, float b)
{
	if (a > b) return b;
	if (a < -b) return -b;
	return a;
}

/**
* This tells the drone how it should move
**/
void AIDrone::setAngles(float pitch, float roll, float yaw, float height)
{
	yaw = saturation(yaw, 1.0f);
	roll = saturation(roll, 1.0f);
	pitch = saturation(pitch, 1.0f);
	height = saturation(height, 1.0f);

    _commands->setPilotInput(roll, pitch, height, yaw);
}

/**
* Returns true if the program should initiate the shutdown procedure
**/
bool AIDrone::requestsExit()
{
    return _requestExit;
}

/**
* A small OpenCV helper function to get the number of bytes per pixel there is in an OpenCV matrix type
**/
inline int typeToBytes(int type)
{
    switch(type)
    {
        case CV_8S:
        case CV_8U:
            return 8;
        case CV_16U:
        case CV_16S:
            return 16;
        case CV_32S:
        case CV_32F:
            return 32;
        case CV_64F:
            return 64;
    }

    return 0;   //should never happen
}

/**
* Tells SDL it should render one frame using the latest data received
**/
void AIDrone::renderFrame()
{
    SDL_RenderClear(_renderer);

    //Has the picture changed?
    if(_videoStream->hasChanged())
    {
        Uint8 *pixels;
        int pitch;
        cv::Mat result;

        //do some image processing magic
        if(_enablePIDRegulator || _showImageProcessing)
        {
            cv::Point2f newestObservedPoint = _imageProcessor.threshold(_videoStream->getCurrentFrame(), result);
            if(isnan(newestObservedPoint.x) || isnan(newestObservedPoint.y))
            {
                if(!_isNanDetected) printf("NaN detected\n");
                _isNanDetected = true;
            }
            else
            {
                _lastObservedHoverPoint = newestObservedPoint;
                _isNanDetected = false;
            }
        }

        if(!_showImageProcessing)
        {
            result = _videoStream->getCurrentFrame();
        }

        //Copy pixel data to SDL texture
        SDL_LockTexture(_texture, nullptr, (void**)&pixels, &pitch);
        memcpy(pixels, result.data, result.size().width * result.size().height * 3); //24 bytes per pixel (8 byte RGB)
        SDL_UnlockTexture(_texture);
//        SDL_UpdateTexture(_texture, nullptr, pixels, pitch);
    }

    SDL_RenderCopy(_renderer, _texture, nullptr, nullptr);

    //Draw yellow rectangle of the area we are trying to center
    if(_enablePIDRegulator)
    {
        SDL_Rect rect;
        rect.w = 60;
        rect.h = 60;
        rect.x = 640/2 - rect.w/2;
        rect.y = 368/2 - rect.h/2;
        SDL_SetRenderDrawColor(_renderer, 255, 255, 0, 128);
        SDL_RenderDrawRect(_renderer, &rect);
    }

    SDL_RenderPresent(_renderer);
}

/**
* Tells the drone it should start flying
**/
void AIDrone::takeoff()
{
	if(!_navdata->hasState(DRONESTATE_FLY_MASK))
	{
        SDL_Delay(100);
		_commands->trim();
		SDL_Delay(200);
		printf("Taking off\n");
		_commands->takeoff();
        SDL_Delay(100);
        _commands->ping();
	}
}

/**
* Tells the drone it should land
**/
void AIDrone::land()
{
//	if(_navdata->hasState(DRONESTATE_FLY_MASK))
	{
        SDL_Delay(100);
        _commands->land();
		SDL_Delay(100);
	}
}

/**
* Main entry point of the program
**/
int main(int argc, char* argv[])
{
    AIDrone drone;

    std::thread updateThread = std::thread([&drone] {
        while(!drone.requestsExit()) drone.update();
    });

	while (!drone.requestsExit())
	{
		drone.renderFrame();
        SDL_Delay(20);
	}

    printf("Exiting gracefully...\n");
    updateThread.join();
	return 0;
}

