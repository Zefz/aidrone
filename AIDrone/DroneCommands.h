#ifndef DRONE_COMMANDS_H_INCLUDED
#define DRONE_COMMANDS_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>

#include <thread>
#include <mutex>
#include <atomic>
#include <initializer_list>

//Forward declaration
class Navdata;

class DroneCommands
{
    public:
        DroneCommands(Navdata *navdata);
        virtual ~DroneCommands();

        void emergencyStop();
        void ping();
        void takeoff();
        void land();
        void trim();
        void setCamera(int cam);
        void setPilotInput(float roll, float pitch, float height, float yaw);
        void setWifiName(const char* name);
        void setRoundelDetectionEnabled(const bool enabled);

        template<typename T>
        void setConfig(const std::string &name, T value);

        void setBuiltInHoverMode(const bool enabled);

    private:
        void commandLoop();
        void bootDrone(int attempt);

        void sendREFCommand(Uint32 bitField);
        void sendCommandRaw(const std::string &command);
        void sendCommand(const std::string &type, std::initializer_list<int> args = {});

    private:
        std::thread _commandThread;
        bool _terminateThread;
        std::mutex _commandMutex;
        Navdata *_navdata;
        bool _disableBuiltinHoverMode;

        std::atomic<Uint32> _sequenceNumber;

        UDPsocket _commandSocket;
        IPaddress _droneAddress;
};

#endif
