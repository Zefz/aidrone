#ifndef DRONECONSTANTS_H_INCLUDED
#define DRONECONSTANTS_H_INCLUDED

/*
 * @enum FLYING_MODE
 * @brief Values for the CONTROL:flying_mode configuration.
 */
enum FLYING_MODE
{
  FLYING_MODE_FREE_FLIGHT = 0,            			/**< Normal mode, commands are enabled */
  FLYING_MODE_HOVER_ON_TOP_OF_ROUNDEL = 1 << 0,  	/**< Commands are disabled, drones hovers on top of a roundel - roundel detection MUST be activated by the user with 'detect_type' configuration. */
  FLYING_MODE_HOVER_ON_TOP_OF_ORIENTED_ROUNDEL = 1 << 1, /**< Commands are disabled, drones hovers on top of an oriented roundel - oriented roundel detection MUST be activated by the user with 'detect_type' configuration. */
};

/**
 * @brief Values for the detection type on drone cameras.
 */
enum CAD_TYPE
{
  CAD_TYPE_HORIZONTAL = 0,           /*<! Deprecated */
  CAD_TYPE_VERTICAL,                 /*<! Deprecated */
  CAD_TYPE_VISION,                   /*<! Detection of 2D horizontal tags on drone shells */
  CAD_TYPE_NONE,                     /*<! Detection disabled */
  CAD_TYPE_COCARDE,                  /*<! Detects a roundel under the drone */
  CAD_TYPE_ORIENTED_COCARDE,         /*<! Detects an oriented roundel under the drone */
  CAD_TYPE_STRIPE,                   /*<! Detects a uniform stripe on the ground */
  CAD_TYPE_H_COCARDE,                /*<! Detects a roundel in front of the drone */
  CAD_TYPE_H_ORIENTED_COCARDE,       /*<! Detects an oriented roundel in front of the drone */
  CAD_TYPE_STRIPE_V,
  CAD_TYPE_MULTIPLE_DETECTION_MODE,  /* The drone uses several detections at the same time */
  CAD_TYPE_CAP,                      /*<! Detects a Cap orange and green in front of the drone */
  CAD_TYPE_ORIENTED_COCARDE_BW,      /*<! Detects the black and white roundel */
  CAD_TYPE_VISION_V2,                /*<! Detects 2nd version of shell/tag in front of the drone */
  CAD_TYPE_TOWER_SIDE,               /*<! Detect a tower side with the front camera */
  CAD_TYPE_NUM                       /*<! Number of possible values for CAD_TYPE */
};

#define DRONE_IP_ADDRESS  "192.168.1.1"
#define REFRESH_MS        28
#define VIDEO_STREAM_PORT 5555

#endif // DRONECONSTANTS_H_INCLUDED
