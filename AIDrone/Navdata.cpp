#include <SDL2/SDL.h>
#include <ctime>
#include <sstream>

#include "Navdata.h"
#include "DroneConstants.h"

/* Navdata constant */
#define NAVDATA_SEQUENCE_DEFAULT  1
#define NAVDATA_PORT              5554
#define NAVDATA_HEADER            0x55667788
#define NAVDATA_BUFFER_SIZE       2048

//The different types of Navdata we can receive
enum NavdataHeaderTag
{
    NAVDATA_DEMO_TAG = 0,
    NAVDATA_VISION_DETECT_TAG = 16,
    NAVDATA_IPHONE_ANGLES_TAG = 18,
    NAVDATA_CKS_TAG = 0xFFFF
};

/* Private variables */
static navdata_unpacked_t _navdata_unpacked;

Navdata::Navdata()
{
    _navdataSocket = nullptr;

    //Reset drone telemetry
    _navdataChanged = false;
    vx = vy = vz = 0.0f;
    phi = theta = psi = 0.0f;
    altitude = 0.0f;
    battery = 0.0f;

    //Open a file we are going to log to
    std::stringstream stringBuilder;
    time_t rawTime = time(nullptr);
    std::tm *time = localtime(&rawTime);

    stringBuilder << "logs/logdata_";
    stringBuilder << (time->tm_mon+1) << '-' << time->tm_mday;
    stringBuilder << '_' << (time->tm_hour+1) << ':' << time->tm_min << ':' << time->tm_sec << ".txt";

    _logStatistics.open(stringBuilder.str());
    _logStatistics.precision(4);

    //Print out the header
    _logStatistics << "timestamp, xv, yv, zv, yaw, pitch, roll, altitude, left-right-tilt, front-back-tilt, vertical_speed, angular_speed, marker_xpos, marker_ypos\n";
    _logStatistics.flush();

    //Start thread
    _terminateThread = false;
    _navdataThread = std::thread(&Navdata::navdataLoop, this);
}

Navdata::~Navdata()
{
    _terminateThread = true;
    _navdataThread.join();

    _logStatistics.close();
}

/**
* This returns true if the navdata has changed since it was last printed
**/
bool Navdata::hasChanged()
{
    return _navdataChanged;
}

/**
* This is used by the logger to print some useful data
**/
void Navdata::printNavdataInfo(float leftRightTilt, float frontBackTilt, float verticalSpeed, float angularSpeed, float lastMarkerX, float lastMarkerY)
{
//    printf("Angles %.2f %.2f %.2f \n", phi, psi, theta);
//    printf("Speeds %.2f %.2f %.2f \n", vx, vy, vz);
//    printf("Battery %.0f \n", battery);
    _navdataChanged = false;
    _logStatistics << SDL_GetTicks() << ", ";
    _logStatistics << vx << ", ";
    _logStatistics << vy << ", ";
    _logStatistics << vz << ", ";
    _logStatistics << psi << ", ";
    _logStatistics << theta << ", ";
    _logStatistics << phi << ", ";
    _logStatistics << altitude << ", ";
    _logStatistics << leftRightTilt << ", ";
    _logStatistics << frontBackTilt << ", ";
    _logStatistics << verticalSpeed << ", ";
    _logStatistics << angularSpeed << ", ";
    _logStatistics << lastMarkerX << ", ";
    _logStatistics << lastMarkerY << '\n';
    _logStatistics.flush();
}

/**
* This unpacks a single navdata option of the specified size into a data buffer
**/
static inline uint8_t* navdata_unpack_option(uint8_t* navdata_ptr, uint8_t* data, uint32_t size)
{
    memcpy(data, navdata_ptr, size);
    return (navdata_ptr + size);
}

/**
* This computes the checksum of a navdata packet
**/
static inline Uint32 navdata_compute_cks(uint8_t* nv, int32_t size)
{
    Uint32 cks = 0;
    for(int i = 0; i < size; i++) cks += nv[i];
    return cks;
}

#define navdata_unpack( navdata_ptr, option ) (navdata_option_t*) navdata_unpack_option( (uint8_t*) navdata_ptr, \
                                                                                         (uint8_t*) &option, \
                                                                                         navdata_ptr->size )

/**
* This decodes a Navdata packet and gets the checksum from the packet
**/
void Navdata::decodeNavdataPacket(navdata_t* packet, Uint32* checksum)
{
    navdata_cks_t navdata_cks = { 0 };
    navdata_option_t* navdata_option_ptr;

    navdata_option_ptr = (navdata_option_t*) &packet->options[0];

    //Reset packet data
    memset(&_navdata_unpacked, 0, sizeof(_navdata_unpacked));

    _navdata_unpacked.drone_state   = packet->drone_state;
    _navdata_unpacked.vision_defined  = packet->vision_defined;

    //Decode each option
    while(navdata_option_ptr != nullptr)
    {
        // Check if we have a valid option
        if( navdata_option_ptr->size == 0 )
        {
            printf("One option is not a valid because its size is zero\n");
            navdata_option_ptr = nullptr;
        }
        else
        {
            switch( navdata_option_ptr->tag )
                {
                case NAVDATA_DEMO_TAG:
                    navdata_option_ptr = navdata_unpack(navdata_option_ptr, _navdata_unpacked.navdata_demo);
                    break;

                case NAVDATA_IPHONE_ANGLES_TAG:
                    navdata_option_ptr = navdata_unpack(navdata_option_ptr, _navdata_unpacked.navdata_iphone_angles);
                    break;

                case NAVDATA_VISION_DETECT_TAG:
                    navdata_option_ptr = navdata_unpack(navdata_option_ptr, _navdata_unpacked.navdata_vision_detect);
                    break;

                case NAVDATA_CKS_TAG:
                    navdata_option_ptr = navdata_unpack(navdata_option_ptr, navdata_cks);
                    *checksum = navdata_cks.cks;
                    navdata_option_ptr = nullptr;
                    break;

                default:
                    printf("Tag %d is not a valid navdata option tag\n", (int) navdata_option_ptr->tag);
                    navdata_option_ptr = nullptr;
                    break;
                }
        }
    }
}

/**
* Check if the drone has the specified bit set according to the last received
* Navdata packet
**/
bool Navdata::hasState(DroneStateBits bit)
{
    return (_droneState & bit) ? true : false;
}

/**
* This function tells the Drone we are ready to receive some navdata packets
**/
void Navdata::pingNavdata()
{
	const int sendstuff = 1;

    if(!_navdataSocket) return;
    printf("Initiating Navdata stream...\n");

	UDPpacket* packet = SDLNet_AllocPacket(NAVDATA_BUFFER_SIZE);
    SDLNet_ResolveHost(&packet->address, DRONE_IP_ADDRESS, NAVDATA_PORT);
    memcpy(packet->data, &sendstuff, sizeof(sendstuff));
    packet->len = 5;
    if(SDLNet_UDP_Send(_navdataSocket, -1, packet) <= 0)
    {
        printf("Unable to send packet: %s\n", SDLNet_GetError());
        return;
    }

    SDLNet_FreePacket(packet);
}

/**
* This is the main navdata loop. Should be run in a separate thread
**/
void Navdata::navdataLoop()
{
	Uint32 currentSequence = NAVDATA_SEQUENCE_DEFAULT-1;
	int navdataPacketsReceived = 0;
	Uint32 startTime = 0;

	printf("NAVDATA thread starting...\n");

    //Create listening UDP socket
    _navdataSocket = SDLNet_UDP_Open(NAVDATA_PORT);
	if(!_navdataSocket)
	{
        printf("Navdata error socket: %s\n", SDLNet_GetError());
        return;
	}

	//Tell the drone we are ready to receive navdata
    pingNavdata();

    //The main loop
	UDPpacket* packet = SDLNet_AllocPacket(NAVDATA_BUFFER_SIZE);
	while(!_terminateThread)
	{
        //Try to receive a packet
		switch(SDLNet_UDP_Recv(_navdataSocket, packet))
		{
            //Error!
            case -1:
                printf("Receive packet error: %s\n", SDLNet_GetError());
            continue;

            //Nothing received, wait a while before checking again
            case 0:
                SDL_Delay(REFRESH_MS);
            continue;

            //Successfully received a packet
            case 1:
                navdata_t* navdata = (navdata_t*) packet->data;

                if(navdata->header != NAVDATA_HEADER)
                {
                    //If it's an control ACK, ignore
                    if(packet->len == 24)
                    {
                        if(packet->data[0] == 137 && packet->data[1] == 119 && packet->data[2] == 102 && packet->data[3] == 85)
                        {
                         //   printf("Got ack!\n");
                            continue;
                        }
                    }
                    printf("Navdata header mismatch! Dropping packet\n");
                    continue;
                }

                //Begin timer from first packet received
                if(startTime == 0) startTime = SDL_GetTicks();

                //Update drone state from navdata
                _droneState = navdata->drone_state;
                if(hasState(DRONESTATE_COM_WATCHDOG_MASK))
                {
                    currentSequence = NAVDATA_SEQUENCE_DEFAULT-1;
                }

                //Check if the sequence number is correct
                if(navdata->sequence <= currentSequence)
                {
                    printf("[Navdata] Sequence pb : %d (distant) / %d (local)\n", navdata->sequence, currentSequence);
                    continue;
                }

                if(!hasState(DRONESTATE_NAVDATA_DEMO_MASK)) continue;

                //Decode packet and compute checksum
                Uint32 navdataChecksum;
                decodeNavdataPacket(navdata, &navdataChecksum);

                //Make sure the checksum matches
                Uint32 checksum = navdata_compute_cks(packet->data, packet->len-sizeof(navdata_cks_t));
                if(checksum != navdataChecksum)
                {
                    printf("Navdata checksum mismatch, dropping packet\n");
                    continue;
                }

                //Get the unpacked data from the navdata packet
                phi         = _navdata_unpacked.navdata_demo.phi;       //roll      ca 0 = horizontal -180000 (left) to 180000 (right)
                psi         = -_navdata_unpacked.navdata_demo.psi;      //yaw       -180000 (left) to 180000 (right)
                theta       = _navdata_unpacked.navdata_demo.theta;     //pitch     ca 0 = horizontal -90000 (down) to 90000 (up)
                altitude    = _navdata_unpacked.navdata_demo.altitude;  //milimeters over the ground (only in flying mode); max 5 meters
                battery     = _navdata_unpacked.navdata_demo.vbat_flying_percentage;
                vx          = _navdata_unpacked.navdata_demo.vx;
                vy          = -_navdata_unpacked.navdata_demo.vy;
                vz          = _navdata_unpacked.navdata_demo.vz;

                if(hasState(DRONESTATE_EMERGENCY_MASK))
                {
                    printf("Emergency mode enabled!\n");
                }

                //Useful while debugging
                if(hasState(DRONESTATE_NOT_ENOUGH_POWER) || battery < 20)
                {
                    printf("WARNING: low battery! (less than 20%%)\n");
                }

                currentSequence = navdata->sequence;
                _navdataChanged = true;
                navdataPacketsReceived++;
		}

	}

    //Free network resources
    SDLNet_FreePacket(packet);
    SDLNet_UDP_Close(_navdataSocket);

    float seconds = (SDL_GetTicks()-startTime) / 1000.0f;
	printf("NAVDATA thread stopping (packets per second %f over %f seconds)\n", navdataPacketsReceived / seconds, seconds);
}
