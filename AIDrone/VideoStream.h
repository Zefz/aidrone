#ifndef CIMAGECLIENT_H
#define CIMAGECLIENT_H

#include <opencv2/opencv.hpp>
#include <SDL2/SDL_net.h>
#include <thread>

//This code snippet adds FFMPEG compatability to 64-bit C++ compiler (else you will get compile error in common.h)
#ifndef INT64_C
#define INT64_C(c) (c ## LL)
#define UINT64_C(c) (c ## ULL)
#endif

extern "C" {
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
}

class VideoStream
{
public:
    VideoStream();
    ~VideoStream();

    cv::Mat getCurrentFrame();
    bool hasChanged();

private:
    void videoLoop();
    int receiveBlocking(TCPsocket socket, Uint8* byteBuffer, const int waitForBytes);
    void decodeFrame(Uint8* byteBuffer, const int len, const int type);

private:
    TCPsocket _videoSocket;

    std::thread _thread;
    bool _terminateThread;

    //Codec stuff
    AVCodec *_codec;
    AVCodecContext *_c;
    AVFrame *_picture;
    AVFrame *_pictureRGB;
    AVPacket _avpkt;
    struct SwsContext *_img_convert_ctx;
    int _srcX, _srcY, _dstX, _dstY;

    //Video frames
    cv::Mat _currentFrame;          ///The latest image frame we have received
    bool _frameHasChanged;          ///Has the video frame changed lately?
    int _framesReceived;            ///How many frames have we decoded in total?
    Uint32 _streamTimeStart;        ///Time in ms when stream was established
};

#endif
