#include "PIDController.h"

/**
* Constructor for PIDController.
* kIntegral is is multiplied by time in seconds.
* kDeriv is divided by time in seconds.
**/
PIDController::PIDController(double kProp, double kIntegral, double kDeriv)
{
    _kProp=kProp;
    _kIntegral=kIntegral;
    _kDeriv=kDeriv;
    _running=false;
}

PIDController::~PIDController()
{
    //dtor
}

/**
* return controller output for error at given time.
* Error is defined as setPoint - measured Variable
**/
double PIDController::controllerOutputAtTime(double error, Uint32 currentTime)
{

    if(!_running)
    {
        _integral=0.0;
        _prevError=error;
        _prevTime=currentTime;
        _running=true;
    }
    double dT=(currentTime-_prevTime)/1000.0;
    _integral += _kIntegral*error*dT;
    double deriv= (dT>0)?(_kDeriv*(error-_prevError)/dT):0;
    _prevTime=currentTime;
    _prevError=error;
    return _kProp*error+_integral+deriv;
}

/**
* return controller output for given error at current time.
* Error is defined as setPoint - measured Variable
**/
double PIDController::controllerOutput(double error)
{
    return controllerOutputAtTime(error, SDL_GetTicks());
}
