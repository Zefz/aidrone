
#Require user confirmation first
while true; do
    read -p "This will install ARDrone and OpenCV requirements. Proceed? (Y/n)" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) echo -e "\033[31m Aborted"; return;;
        * ) echo "Please answer yes or no.";;
    esac
done

#Install FFMPEG dependencies
echo -e "\033[32m Installing FFMPEG";
tput sgr0
sudo apt-get install ffmpeg

#Install SDL 2.0
echo -e "\033[32m Installing SDL 2.0";
tput sgr0
sudo add-apt-repository ppa:pj-assis/testing
sudo apt-get update
sudo apt-get install libsdl2-dev

#Now install all OpenCV dependencies
echo -e "\033[32m Installing OpenCV";
tput sgr0
sudo apt-get install libopencv-dev
sudo apt-get install libopencv-highgui-dev
sudo apt-get install libcv-dev
sudo apt-get install libhighgui-dev

